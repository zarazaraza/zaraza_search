import os
import pickle as pkl
import random
import time
import datetime

import telethon as tl
from telethon.errors import FloodWaitError

client = tl.TelegramClient(os.environ['SESSION_NAME'], os.environ["API_ID"], os.environ["API_HASH"])


def generate_string():
    symbols = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'
    return "".join([random.choice(symbols) for _ in range(96)])


async def main():
    print("MAIN", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    count = 0
    with open(os.environ['TEMP_FILE'], "r") as f:
        answers = {}

        for j, link in enumerate(f.read().strip().split()):
            count += 1
            time.sleep(1)
            last = link.strip()[1:]
            try:
                if last not in answers:
                    answers[last] = []
                print(last)
                async for message in client.iter_messages(last, wait_time=1):
                    answers[last].append(
                        {
                            'message' : message.message,
                            'date' : message.date,
                            'id' : message.id,
                            'channel_id' : message.to_id.channel_id
                        }
                    )
                if count % 10 == 0:
                    time.sleep(10)
            except FloodWaitError as e:

                client._flood_sleep_threshold = 10
                print("wait", e.seconds)
                time.sleep(10)
            except Exception as e:
                print(e)
            if len(answers) == 10:
                new_file = generate_string()
                pkl.dump(answers,
                         open(os.environ['SAVE_FILE'] + new_file + ".pkl", "wb"))
                answers = {}
        if len(answers) != 0:
            new_file = generate_string()
            pkl.dump(answers,
                     open(os.environ['SAVE_FILE'] + new_file + ".pkl", "wb"))


client._flood_sleep_threshold = 0

with client:
    client.loop.run_until_complete(main())
