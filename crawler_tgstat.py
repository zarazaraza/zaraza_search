import requests
from urllib.parse import quote
from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.parse
import os

file_c = 0
prefixes = [
    "https://tgstat.ru",
    "https://uk.tgstat.com",
    "https://by.tgstat.com",
    "https://kaz.tgstat.com",
    "https://en.tgstat.com",
    "https://tgstat.com",
    "//tgstat.ru",
    "//uk.tgstat.com",
    "//by.tgstat.com",
    "//kaz.tgstat.com",
    "//en.tgstat.com",
    "//tgstat.com",
"https://tgstat.ru",
    "https://uk.tgstat.com/ru",
    "https://by.tgstat.com/ru",
    "https://kg.tgstat.com/ru",
    "https://en.tgstat.com/ru",
    "https://tgstat.com/ru",
    "//tgstat.ru/",
    "//uk.tgstat.com/ru",
    "//by.tgstat.com/ru",
    "//kaz.tgstat.com/ru",
    "//en.tgstat.com/ru",
    "//tgstat.com/ru",
]

def download(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return response.content
        else:
            return None
    except:
        return None

def parse_html(url):
    content = download(url)
    if content is None:
        return []

    html = BeautifulSoup(content, "html.parser")

    links = html.find_all("a")
    results = set([])
    for link in links:
        href = link.get("href")
        if href is None:
            continue
        for prefix in prefixes:
            if href.startswith(prefix):
                results.add(href)
                break
    return results

visited = set([])
def parse_urls(url, start, max_deep=6):
    global file_c
    res = []
    if start >= max_deep:
        return []
    visited.add(url)
    print(url, start)
    for to in parse_html(url):
        if to not in visited:
            for prefix in prefixes:

                if to.startswith(prefix + "/channel/@") and to.split("/")[-1][0] == '@' and to.split("/")[-1] not in visited:
                    visited.add(to)
                    visited.add(to.split("/")[-1])
                    res.append(to)
                    print("added", to)
                    break

            res += parse_urls(to, start + 1, max_deep)
            if len(res) > 1000:
                file_c += 1
                with open("tgstat/tgstat" + str(file_c) + ".txt", "w") as f:
                    f.write("\n".join(res))
                res = []

    return res

main_urls = ["https://tgstat.com/ru/channel/@ProxyMTProto"]
result = []
for url in main_urls:
    result += parse_urls(url, 0, 15)

with open("tgstat/tgstat.txt", "w") as f:
    f.write("\n".join(result))