FROM dhna/mpi4py:latest

RUN apt-get update && \
    apt-get install -y -q \
        g++ \
        python3-dev \
        libffi-dev \
        python3 \
        libcr-dev && \
    rm -rf /var/lib/apt/lists/* && \
    pip3 install --upgrade pip setuptools && \
    pip3 install nltk telethon && \
    python -m nltk.downloader stopwords

RUN apt-get update && \ 
    apt-get install -y \
        cron \
        rsyslog && \
    rm -rf /var/lib/apt/lists/* && \
    python -m nltk.downloader punkt

RUN apt-get update -y && \
    apt-get install -q -y \
        mpich \
        mpich-doc && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update -y && \
    apt-get install -y \
        curl && \
        rm -rf /var/lib/apt/lists/*

COPY ./preprocessing.py /srv/preprocessing.py
COPY ./extract_messages_temp_file.py /srv/extract_messages_temp_file.py
COPY ./preprocessing.sh /srv/preprocessing.sh
COPY ./cron_entrypoint.sh /srv/entrypoint.sh

COPY ./tg_cron.sh /srv/tg_cron.sh
COPY ./cron /etc/cron.d/reindex

WORKDIR /srv
CMD /bin/bash entrypoint.sh
