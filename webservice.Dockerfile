FROM python:3.6-alpine

RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev openssl-dev && \
    apk add --no-cache --update python3 && \
    pip3 install --upgrade pip setuptools && \
    pip3 install flask nltk && \
    rm -rf /root/.cache && \
    rm -rf /var/cache/apk/*

RUN python3 -m nltk.downloader stopwords
RUN python3 -m nltk.downloader punkt

EXPOSE 8000

COPY ./app.py /srv/app.py

ENV FLASK_APP /srv/app.py
ENV FLASK_RUN_PORT 8000

WORKDIR /srv
CMD ["python3", "/srv/app.py"]