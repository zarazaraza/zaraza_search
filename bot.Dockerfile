FROM python:3.6-alpine

RUN apk add --no-cache --virtual g++ python3-dev libffi-dev && \
    apk add --no-cache --update python3 && \
    pip3 install --upgrade pip setuptools && \
    pip3 install pyTelegramBotAPI requests urllib3 && \
    rm -rf /root/.cache && \
    rm -rf /var/cache/apk/*

COPY ./bot.py /srv/bot.py

WORKDIR /srv
CMD ["python3", "/srv/bot.py"]
