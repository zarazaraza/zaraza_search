import os
import time
import pickle as pkl
# import nmslib

from flask import Flask, request, jsonify, redirect, render_template

from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from math import log2

# from gensim.models.doc2vec import Doc2Vec
# from annoy import AnnoyIndex
print("START WORDS")
words = pkl.load(open(os.environ['WORDS'], "rb"))
print("START DOCUMENTS")
documents = pkl.load(open(os.environ['DOCUMENTS'], "rb"))
print("START DOCUMENTS_LENGTH")
doc_length = pkl.load(open(os.environ['DOCUMENTS_LENGTH'], "rb"))
eng_stem = SnowballStemmer('russian')
rus_stem = SnowballStemmer('english')
s_words = set(stopwords.words('kazakh')).union(
    set(stopwords.words('english')).union(
        set(stopwords.words('russian'))))


# doc2vec = Doc2Vec.load('model/doc2vec.model')
# s_index = AnnoyIndex(100, 'euclidean')
# s_index.load('model/model.ann')


def preprocess_query(query):
    words = word_tokenize(query.lower())
    res = []
    for word in words:
        if word not in s_words and word.isalnum():
            res.append(
                rus_stem.stem(word) if rus_stem.stem(word) != word
                else eng_stem.stem(word))
    return res


def okapi_scores(query, offset, limit, k1=1.2, b=0.75):
    scores = {}
    st = time.time()
    all_doc_length = sum(doc_length.values())
    for key in preprocess_query(query):
        if key not in words:
            continue
        asd = words[key]
        idf = log2(
            (len(doc_length) - len(asd) + 1 + 0.5)
            / (len(asd) - 1 + 0.5))
        count = 0
        for t in asd.keys():
            if t not in scores:
                scores[t] = 0.
            scores[t] += idf * asd[t] * (k1 + 1) / (
                    asd[t] + k1 * (
                    1. - b + b * doc_length[t] / (
                    1. * all_doc_length / len(
                doc_length))))
            count += 1
            if count % 100 == 0:
                # print(time.time() - st)
                st = time.time()

    return sorted(scores.items(), key=lambda x: -x[1])[offset:offset + limit]


# def nmslib_scores(query, offset, limit):
#     query = preprocess_query(query)
#     query = doc2vec.infer_vector(query)
#     ids = second_index.knnQuery(query, k = offset + limit)
#     return ids

app = Flask(__name__)


@app.route('/update', methods=['GET'])
def update():
    global flag, words, documents, doc_length
    if request.args.get('secret') == '123':

        temp_words = pkl.load(open(os.environ['WORDS'], "rb"))
        temp_documents = pkl.load(open(os.environ['DOCUMENTS'], "rb"))
        temp_doc_length = pkl.load(open(os.environ['DOCUMENTS_LENGTH'], "rb"))
        words = temp_words
        documents = temp_documents
        doc_length = temp_doc_length

        return jsonify({'message':'updated'})
    return jsonify({'message':'error'})


@app.route('/add_channel', methods=["GET"])
def add_channel():
    if request.args.get('channel') is not None:

        channel = request.args.get('channel').strip()
        if not os.path.exists('temp.txt'):
            with open("temp.txt", 'w') as f:
                f.write("")

        with open('temp.txt', 'r') as f:
            channels = f.read().split()
            if channel in channels:
                return jsonify({
                    "message": "your channel " + channel + " in waiting list"
                })
            if len(channels) > 100:
                return jsonify({
                    "message": "Try next time"
                })
        with open('temp.txt', 'a') as f:
            f.write(channel + "\n")
        return jsonify({
            "message": "we added your channel " + channel
        })
    else:
        return jsonify({"message": "Wrong query"})


# def annoy_scores(query, offset, limit):
#     query = preprocess_query(query)
#     vec = doc2vec.infer_vector(query)
#     return s_index.get_nns_by_vector(vec, n = offset + limit)


@app.route('/search', methods=['GET'])
def search():
    offset = 0
    limit = 10
    if request.args.get('offset') is not None:
        offset = int(request.args.get('offset'))
    if request.args.get('limit') is not None:
        limit = int(request.args.get('limit'))

    if request.args.get('q') is not None:
        query = request.args.get('q')
        st = time.time()
        scores = okapi_scores(query, offset, limit)
        # ids = annoy_scores(query, offset, limit)
        # print(ids)
        res = [documents[key] for key, _ in scores]
        # res2 = [documents[key] for key in ids]
        return jsonify({'result': res})
    else:
        return jsonify({'error': 'no arg q'})


# @app.route('/')
# def main():
#     return render_template('main.html', name='Main Page')


if __name__ == '__main__':
    app.run(host="0.0.0.0", port="8000")
