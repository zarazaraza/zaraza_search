import os
import pickle as pkl
import time

from mpi4py import MPI
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer, PorterStemmer



s_words = set(stopwords.words('kazakh')).union(set(stopwords.words('english')).union(set(stopwords.words('russian'))))
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    st = time.time()

def clean_data(x):
    stem_rus = SnowballStemmer('russian')
    stem_eng = PorterStemmer()
    if x['message'] == None:
        return ""
    words = word_tokenize(x['message'].lower())
    res = []
    for word in words:
        if word not in s_words and word.isalnum():
            res.append(stem_rus.stem(word) if stem_rus.stem(word) != word else stem_eng.stem(word))
    return " ".join(res)

if rank == 0:

    files = os.listdir(os.environ["FOLDER_WITH_CHANNELS"])
    send = [[] for _ in range(size)]
    for i, file in enumerate(files):
        send[i % size].append(os.environ["FOLDER_WITH_CHANNELS"] + "/" + file)
    del files
else:
    send = None


files = comm.scatter(send, root=0)
print(len(files))
filtered = []
for file in files:
    data = pkl.load(open(file, "rb"))
    for key in data.keys():
        for obj in data[key]:
            obj['channel'] = key
            del obj['channel_id']
            del obj['date']
            filtered.append((obj, clean_data(obj)))

# for i in range(100):
#     print(rank,'filtered', filtered[i][1], ' -> original', filtered[i][0]['message'])
count = len(filtered)
count = comm.gather(count, root=0)

if rank == 0:
    start_indexes = [0]
    for i in range(len(count) - 1):
        start_indexes.append(sum(count[:i + 1]))
    del count
else:
    start_indexes = []
    del count

start_indexes = comm.scatter(start_indexes, root=0)
print(start_indexes)
documents = {}
words = {}
documents_length = {}
for index, (obj, filtered_text) in \
        zip(range(start_indexes, start_indexes + len(filtered)), filtered):
    documents[index] = obj
    size = 0
    for word in word_tokenize(filtered_text):
        size += 1
        if word not in words:
            words[word] = {}
        if index not in words[word]:
            words[word][index] = 0
        words[word][index] += 1

    documents_length[index] = size

print(rank, 'length', len(documents))
print(rank, 'length', len(documents_length))
print(rank, 'length', len(words))


del filtered
all_doc = comm.gather(documents, root=0)
del documents
all_doc_len = comm.gather(documents_length, root=0)
del documents_length
all_words = comm.gather(words, root=0)
del words

if rank == 0:

    if not os.path.exists('data'):
        os.mkdir('data')
    final_words = {}
    final_documents = {}
    final_documents_length = {}
    for i in range(len(all_doc)):
        for key in all_doc[i].keys():
            final_documents[key] = all_doc[i][key]
        all_doc[i] = None
        for key in all_doc_len[i]:
            final_documents_length[key] = all_doc_len[i][key]
        all_doc_len[i] = None
        for key in all_words[i]:
            if key not in final_words:
                final_words[key] = all_words[i][key]
            else:
                for index in all_words[i][key]:
                    final_words[key][index] = all_words[i][key][index]
        all_words[i] = None

    print("LAST", rank, len(final_words.keys()))
    print("LAST", rank, len(final_documents.keys()))
    print("LAST", rank, len(final_documents_length.keys()))

    pkl.dump(final_words, open(os.environ["WORDS"], "wb"))
    pkl.dump(final_documents, open(os.environ["DOCUMENTS"], "wb"))
    pkl.dump(final_documents_length, open(os.environ["DOCUMENTS_LENGTH"], "wb"))
    print("TIME: ", time.time() - st)

