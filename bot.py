import telebot as tg
import os
import requests
import json
import time
import random
import string

from urllib.parse import urlencode

service_url = os.environ['SERVICE_URL']
bot = tg.TeleBot(os.environ['BOT_TOKEN'])

hash_param = {}


def generate_string():
    symbols = string.ascii_lowercase + string.ascii_uppercase \
              + string.digits + string.punctuation
    print(symbols)
    return "".join([random.choice(symbols) for _ in range(32)])


def create_request(query, offset=0, limit=10):
    params = {
        'q': query,
        'offset': offset,
        'limit': limit
    }
    print(urlencode(params))
    response = requests.get(service_url + '/search',
                            params=urlencode(params))
    return response


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    obj = hash_param[call.data]
    del hash_param[call.data]
    response = create_request(
        obj['q'],
        obj['offset'] + obj['limit'],
        obj['limit']
    )
    if response.status_code == 200:
        result = json.loads(response.content.decode('utf-8'))
        del response
        if 'error' in result:
            bot.send_message(
                call.from_user.id,
                "Something wrong. Try next time"
            )
        elif len(result['result']) > 0:

            for i in range(len(result['result'])):
                link = 'https://t.me/' + result['result'][i]['channel'] + '/' \
                       + str(result['result'][i]['id'])
                if i == 9:
                    params = {
                        'q': obj['q'],
                        'offset': obj['offset'] + obj['limit'],
                        'limit': 10
                    }
                    hash = generate_string()
                    hash_param[hash] = params
                    keyboard = tg.types.InlineKeyboardMarkup()
                    keyboard.add(tg.types.InlineKeyboardButton(
                        text="Купить",
                        callback_data=hash
                    ))
                    bot.send_message(
                        call.from_user.id,
                        result['result'][i]['message'] + '\n' + link,
                        reply_markup=keyboard
                    )
                else:
                    bot.send_message(
                        call.from_user.id,
                        result['result'][i]['message'] + '\n' + link,
                    )
                time.sleep(0.4)

        else:
            bot.send_message(call.from_user.id, "Sorry! Try another!")
    else:
        bot.send_message(call.from_user.id, "Something wrong. Try next time")


@bot.message_handler(commands=["start"])
def start(message):
    bot.send_message(message.chat.id, "run command: /search :your text:")


@bot.message_handler(commands=["search"])
def search(message):
    query = message.text.split()
    pr_query = []
    for i in range(1, len(query)):
        if len(query[i]) != 0:
            pr_query.append(query[i])
    query = " ".join(pr_query)
    response = create_request(query)
    if response.status_code == 200:
        result = json.loads(response.content.decode('utf-8'))
        del response
        if 'error' in result:
            bot.send_message(message.chat.id, "Something wrong. Try next time")
        elif len(result['result']) > 0:

            for i in range(len(result['result'])):
                link = 'https://t.me/' + result['result'][i]['channel'] + '/' \
                       + str(result['result'][i]['id'])
                if i == 9:
                    params = {
                        'q': query,
                        'offset': 0,
                        'limit': 10
                    }
                    hash = generate_string()
                    hash_param[hash] = params
                    keyboard = tg.types.InlineKeyboardMarkup()
                    keyboard.add(tg.types.InlineKeyboardButton(
                        text="Show More",
                        callback_data=hash)
                    )
                    bot.send_message(
                        message.chat.id,
                        result['result'][i]['message'] + '\n' + link,
                        reply_markup=keyboard
                    )
                else:
                    bot.send_message(
                        message.chat.id,
                        result['result'][i]['message'] + '\n' + link,
                    )
                time.sleep(0.4)

        else:
            bot.send_message(message.chat.id, "Sorry! Try another!")
    else:
        bot.send_message(message.chat.id, "Something wrong. Try next time")


@bot.message_handler(commands=['add_channel'])
def add_channel(message):
    channel = message.text.split()
    if len(channel) > 2 or channel[-1][0] != '@':
        bot.send_message(
            message.chat.id,
            "command for add and update channel /add_channel @your_channel"
        )
    else:
        params = {
            'channel': channel[1]
        }
        print(urlencode(params))
        response = requests.get(service_url + '/add_channel',
                                params=urlencode(params))
        bot.send_message(message.chat.id, json.loads(response.content.decode('utf-8'))['message'])


if __name__ == '__main__':
    bot.polling(none_stop=True)
