echo "
export API_HASH=${API_HASH}
export SESSION_NAME=${SESSION_NAME}
export API_ID=${API_ID}
export SAVE_FILE=${SAVE_FILE}
export TEMP_FILE=${TEMP_FILE}
export SERVICE_URL=${SERVICE_URL}
export WORDS=${WORDS}
export DOCUMENTS=${DOCUMENTS}
export DOCUMENTS_LENGTH=${DOCUMENTS_LENGTH}
cd /srv
python3 /srv/extract_messages_temp_file.py

/srv/preprocessing.sh

echo \"\" > /srv/temp.txt" >> /srv/tg_cron.sh
service rsyslog start && service cron start && tail -f /var/log/syslog