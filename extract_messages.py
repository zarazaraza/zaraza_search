import asyncio
import telethon as tl
import os
import json
import time
import shelve
import pickle as pkl
from telethon.errors import FloodWaitError


client = tl.TelegramClient(os.environ['SESSION_NAME'], os.environ["API_ID"], os.environ["API_HASH"])
async def main():
    for i, file in enumerate(sorted(os.listdir('tgstat'))[int(os.environ['START']):int(os.environ['END'])]):
        with open('tgstat/' + file, "r") as f:
            count = 0
            answers = {}
            for j, link in enumerate(f):
                if i < 1 or (i <= 1 and j < 170):
                    continue
                count += 1
                time.sleep(1)
                last = link.split("/")[-1].strip()[1:]
                print(i, j, last, count)
                try:
                    if last not in answers:
                        answers[last] = []
                    async for message in client.iter_messages(last, limit=1000, wait_time=10):
                        answers[last].append(
                            {
                                'message' : message.message,
                                'date' : message.date,
                                'id' : message.id,
                                'channel_id' : message.to_id.channel_id
                            }
                        )
                    if count % 10 == 0:
                        time.sleep(10)
                except FloodWaitError as e:

                    client._flood_sleep_threshold = 10
                    print("wait", e.seconds)
                    time.sleep(10)
                except Exception as e:
                    print(e)
                if len(answers) == 10:
                    pkl.dump(answers, open("datasets_channel/" + os.environ["SESSION_NAME"] + str(i) + "_" + str(j) +"_" + str(count) + ".pkl", "wb"))
                    answers = {}





client._flood_sleep_threshold=0
with client:
    client.loop.run_until_complete(main())



